#! /bin/bash

# A lancer en root par sudo dans le répertoire des scripts
# sudo ./ptx-lubuntu.sh

#chemin_iso=~/"Téléchargements/lubuntu-18.04.2-desktop-amd64.iso"
chemin_iso=""
filesys=~/squashfs
# Redirection d'erreurs vers un fichier log
fichierlog="/var/log/ptx-lubuntu.log"
date > "$fichierlog"
exec 2>>"$fichierlog"
err_chroot="$filesys/tmp/operations-chroot.log"
fic_liste_manquants="$filesys/tmp/liste-paquets-manquants.txt"

# Le chemin d'accès à l'iso de Lubuntu a-t-il bien été indiqué ?
if [ ! -e "$chemin_iso" ]; then
  echo "Vous n'avez pas indiqué le chemin d'accès à l'iso,
ou ce chemin est invalide."
  exit 1
fi

# Préparation de l'environnement de travail
apt-get install squashfs-tools schroot xorriso
mkdir "$filesys"
mkdir ~/iso
mkdir /media/lubuntu
mkdir /media/squashfs
mount -o loop -t iso9660 "$chemin_iso" /media/lubuntu
cp -rv /media/lubuntu/. ~/iso
mount -t squashfs -o loop ~/iso/casper/filesystem.squashfs /media/squashfs
cp -av /media/squashfs/. "$filesys"
cp /run/systemd/resolve/stub-resolv.conf "$filesys/run/systemd/resolve/"
umount /media/squashfs
umount /media/lubuntu
rmdir /media/lubuntu
rmdir /media/squashfs
rm ~/iso/casper/filesystem.squashfs
mount --bind /proc "$filesys/proc"
cp operations-chroot.sh "$filesys/tmp"

# On passe dans le nouveau système et on y lance les opérations
chroot "$filesys" /tmp/operations-chroot.sh

# On revient dans le système hôte et on récupère les noyaux système
umount "$filesys/proc"
chmod 777 ~/iso/casper
rm ~/iso/casper/vmlinuz ~/iso/casper/initrd* ~/iso/casper/filesystem.manifest
# S'il y a plusieurs noyaux, on récupère la version la plus récente
cp "$filesys"/boot/$(ls "$filesys"/boot | grep 'vmlinuz-*' | sed '$!d') ~/iso/casper/vmlinuz
cp "$filesys"/boot/$(ls "$filesys"/boot | grep 'initrd.img-*' | sed '$!d') ~/iso/casper/initrd

# On génère le nouveau filesystem
chroot "$filesys" dpkg-query -W --showformat='${Package} ${Version}\n' > ~/iso/casper/filesystem.manifest
chmod go-w ~/iso/casper/filesystem.manifest
cd "$filesys"
mksquashfs . ~/iso/casper/filesystem.squashfs

# Récupère les log lors des opérations de chroot
echo "
=====================================
Erreurs lors des opérations de chroot
=====================================
" >>"$fichierlog"
cat "$err_chroot" >>"$fichierlog"
echo "
=====================================
" >>"$fichierlog"
cp "$fic_liste_manquants" "/tmp/liste-paquets-manquants.txt"

# Nettoyage du répertoire de construction
echo "Le répertoire de construction occupe un espace disque important.
Cela peut empêcher la création de l'iso sur une machine virtuelle si
l'espace disque alloué est insuffisant.
Espace disque libre sur la partition de travail : $(df -h ~/ | awk '/dev/ {print $4}')
Souhaitez-vous effacer le répertoire de travail ? (O/N défaut N)"
read choix
if [ "$choix" = "O" ] || [ "$choix" = "o" ]; then
  rm -rf "$filesys"
fi

# Préparation de l'iso
chmod 777 ~/iso/preseed/lubuntu.seed
echo "# The Lubuntu seeds assume that installation of Recommends is disabled.
d-i base-installer/install-recommends boolean true
# Créer un compte root.
d-i passwd/root-login boolean true
d-i passwd/root-password password tuxprof
d-i passwd/root-password-again password tuxprof
# clavier fr
# d-i keymap select fr
# localhost name
d-i netcfg/get_hostname string primtux
# no normal user
d-i passwd/make-user boolean false
# Enable extras.ubuntu.com.
d-i apt-setup/extras boolean true
# Install the Lubuntu desktop.
tasksel tasksel/first multiselect lubuntu-desktop
# No LXDE translation packages yet.
d-i pkgsel/language-pack-patterns string" > ~/iso/preseed/lubuntu.seed

cd ~/iso
bash -c "find . -path ./isolinux -prune -o -type f -not -name md5sum.txt -print0 | xargs -0 md5sum | tee md5sum.txt"
cd $HOME
wget https://primtux.fr/sources-lubuntu-18.04/isohdpfx.bin

# Construction de l'iso
xorriso -as mkisofs \
-isohybrid-mbr ~/isohdpfx.bin \
-c isolinux/boot.cat \
-b isolinux/isolinux.bin \
-no-emul-boot \
-boot-load-size 4 \
-boot-info-table \
-eltorito-alt-boot \
-e boot/grub/efi.img \
-no-emul-boot \
-isohybrid-gpt-basdat \
-o ~/PrimTux4-Lubuntu-18.04.iso \
~/iso

# Si des paquets n'ont pu être installés, on affiche leur nom
if [ -e "/tmp/liste-paquets-manquants.txt" ]; then
  echo "
++++++++++++++++++++++++++++++++++++++++++++++++
Les paquets suivants n'ont pas pu être installés
++++++++++++++++++++++++++++++++++++++++++++++++
"
  else echo "Tous les paquets ont été convenablement installés."
fi
echo "Un fichier log des opérations a été enregistré :
$fichierlog
"
rm "/tmp/liste-paquets-manquants.txt"

exit 0
